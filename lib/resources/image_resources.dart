const imgLoginBg = "assets/images/img_login_bg.png";
const imgSignBack = "assets/images/ic_sign_back.png";
const icUser = "assets/images/ic_user.svg";
const icDeselectEmail = "assets/images/ic_deselect_mail.svg";
const icDeselectLock = "assets/images/ic_deselect_lock.svg";
const icSelectEmail = "assets/images/ic_select_mail.svg";
const icSelectLock = "assets/images/ic_select_lock.svg";

const icLeftArrow = "assets/images/ic_left_arrow.svg";
const icLeftArrowVp = "assets/images/img_home_page/ic_left_arrow_vp.svg";
const icDeselectUser = "assets/images/ic_deselect_user.svg";
const icSelectUser = "assets/images/ic_select_user.svg";
const icDropDown = "assets/images/ic_drop_down.svg";
const icProfileAdd = "assets/images/ic_profile_add.svg";
const imgProfile= "assets/images/img_profile.png";

const imgOtpBackground = "assets/images/ic_otp_backgnd.png";
const imgOtpMsgBackground = "assets/images/ic_otp_msg_backgnd.png";

const imgAppLogo = "assets/images/ic_user.svg";

const imgGoogleSignIn = "assets/images/ic_google_sign_in.svg";
const imgAppleSignIn = "assets/images/ic_apple_sign_in.svg";
const imgFaceBookSignIn = "assets/images/ic_facebook_sign_in.svg";
const imgTwitterSignIn = "assets/images/ic_twitter_sign_in.svg";

//HomePage
const icGhostMode = "assets/images/img_home_page/ic_ghost_mode.svg";
const imgAddPost = "assets/images/img_home_page/img_add_post.png";
const icNotifyBell = "assets/images/img_home_page/ic_notify_bell.svg";
const icCardMenu = "assets/images/img_home_page/ic_menu_bar.svg";
const icVerifyBadge = "assets/images/img_home_page/ic_verify_badge.svg";
const icSharePost = "assets/images/img_home_page/ic_share_post.svg";
const icCommentPost = "assets/images/img_home_page/ic_comment_post.svg";
const icLikeWhite = "assets/images/img_home_page/ic_like_white.svg";
const icLikeRed = "assets/images/img_home_page/ic_like_red.svg";
const icBookMarkReg = "assets/images/img_home_page/ic_bookmark_regular.svg";
const icCloseSheet = "assets/images/img_home_page/ic_close_sheet.svg";
const icStoryCmtSend = "assets/images/img_home_page/ic_story_cmt_send.svg";
const icReportPostDone = "assets/images/img_home_page/ic_post_report_done.svg";
const icIosBackArrow = "assets/images/ic_ios_back_arrow.svg";
const icAddPostRed ="assets/images/img_home_page/ic_add_post_red.svg";
const icRemovePost = "assets/images/img_home_page/ic_remove_post.svg";
const icMusicPlayBtn = "assets/images/img_home_page/ic_music_play.svg";
const icLocationPost ="assets/images/img_home_page/ic_location_post.svg";
const icTagPeople = "assets/images/img_home_page/ic_tag_people.svg";

const icBottomSelHome = "assets/images/img_home_page/ic_btm_sel_home.svg";
const icBottomSelGrp = "assets/images/img_home_page/ic_btm_sel_grp.svg";
const icBottomSelSearch = "assets/images/img_home_page/ic_btm,_sel_search.svg";
const icBottomSelChat = "assets/images/img_home_page/ic_btm_sel_chat.svg";
const icBottomDeSelHome = "assets/images/img_home_page/ic_btm_de_sel_home.svg";
const icBottomDeSelSearch = "assets/images/img_home_page/ic_btm_de_sel_search.svg";
const icBottomDeSelGrp = "assets/images/img_home_page/ic_btm_de_sel_grp.svg";
const icBottomDeSelChat = "assets/images/img_home_page/ic_btm_de_sel_chat.svg";